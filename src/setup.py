from setuptools import setup

setup(name='editdistance',
      version='0.1',
      description='implementation of Levenshtein distance',
      author='Chris Nixon',
      author_email='chris.nixon@sigma.me.uk',
      license='Apache License, Version 2.0',
      packages=['editdistance'],
      )
