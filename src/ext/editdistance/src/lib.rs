use std::cmp::min;

pub fn editdistance(root: &str, query: &str) -> i32 {
    let (s1, s2) = match (root.chars().count(), query.chars().count()) {
        (r, q) if r > q => (query, root),
        _ => (root, query),
    };

    let mut distances: Vec<i32> = (0..(s1.len() as i32 + 1)).collect::<Vec<i32>>();
    for (i2, c2) in s2.chars().enumerate() {
        let mut distances_ = Vec::with_capacity(s1.len() + 1);
        distances_.push(i2 as i32 + 1);
        for (i1, c1) in s1.chars().enumerate() {
            if c1 == c2 {
                distances_.push(distances[i1]);
            } else {
                let last = *distances_.last().unwrap();
                distances_.push(1 + min(min(distances[i1], distances[i1 + 1]),
                                        last))
            }
        }
        distances = distances_
    }
    *distances.last().unwrap()
}
